<?php

namespace tests\Model;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Kisphp\Entity\KisphpEntityInterface;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;
use Prophecy\Prophet;
use tests\Model\Helper\DemoEntity;
use tests\Model\Helper\DemoModel;
use tests\Model\Helper\DemoStatusEntity;

class ModelTest extends TestCase
{
    /**
     * @var DemoModel
     */
    protected $model;

    protected function setUp(): void
    {
        $prophet = new Prophet();
        $entityRepository = $prophet->prophesize(EntityRepository::class);
        $entityRepository->find(Argument::any())->willReturn(new DemoEntity());
        $entityRepository->find(1)->willReturn(new DemoEntity());
        $entityRepository->find(2)->willReturn(new DemoStatusEntity());
        $entityRepository->find(100)->willReturn(null);
        $entityRepository->findAll()->willReturn([]);
        $entityRepository
            ->findBy(
                Argument::any(),
                Argument::any(),
                Argument::any(),
                Argument::any())
            ->willReturn([
                [
                    'id' => 1,
                    'title' => 'alfa',
                ],
                [
                    'id' => 2,
                    'title' => 'beta',
                ],
            ])
        ;
        $entityRepository
            ->findOneBy(Argument::any())
            ->willReturn(
                [
                    'id' => 1,
                    'title' => 'alfa',
                ]
            )
        ;

        $prophet = new Prophet();
        $entityManager = $prophet->prophesize(EntityManagerInterface::class);
        $entityManager->persist(Argument::any())->willReturn(null);
        $entityManager->getRepository(Argument::any())->willReturn($entityRepository->reveal());
        $entityManager->flush()->willReturn(null);
        $entityManager->remove(Argument::any())->willReturn(new DemoEntity());


        $this->model = new DemoModel($entityManager->reveal());
    }

    public function test_find()
    {
        self::assertInstanceOf(KisphpEntityInterface::class, $this->model->find(1));
    }

    public function test_save()
    {
        $entity = new DemoEntity();

        self::assertSame($entity, $this->model->save($entity));
    }

    public function test_findAll()
    {
        self::assertSame([], $this->model->findAll());
    }

    public function test_findBy()
    {
        self::assertSame([
            [
                'id' => 1,
                'title' => 'alfa',
            ],
            [
                'id' => 2,
                'title' => 'beta',
            ],
        ], $this->model->findBy([
            'id' => 1,
        ]));
    }

    public function test_findOneBy()
    {
        self::assertSame([
            'id' => 1,
            'title' => 'alfa',
        ], $this->model->findOneBy([
            'id' => 1,
        ]));
    }

    public function test_remove()
    {
        $entity = new DemoEntity();

        $outEntity = $this->model->remove($entity);

        self::assertInstanceOf(KisphpEntityInterface::class, $outEntity);
    }

    public function test_remove_with_status()
    {
        $entity = new DemoStatusEntity();

        $outEntity = $this->model->remove($entity);

        self::assertInstanceOf(KisphpEntityInterface::class, $outEntity);

        self::assertEquals(0, $outEntity->getStatus());
    }

    public function test_fine_one_by_id()
    {
        $entity = $this->model->findOneOrCreateById(1);

        self::assertInstanceOf(KisphpEntityInterface::class, $entity);
    }

    public function test_fine_one_create_by_id()
    {
        $entity = $this->model->findOneOrCreateById(100);

        self::assertInstanceOf(KisphpEntityInterface::class, $entity);
    }
}
