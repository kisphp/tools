<?php

namespace tests\Model;

use Doctrine\ORM\EntityManagerInterface;
use Kisphp\Exceptions\ConstantNotFound;
use PHPUnit\Framework\TestCase;
use Prophecy\Prophet;
use tests\Model\Helper\NoRepositoryModel;

class NoRepositoryModelTest extends TestCase
{
    public function test_exception()
    {
        $prophet = new Prophet();
        $this->expectException(ConstantNotFound::class);

        $entityManger = $prophet->prophesize(EntityManagerInterface::class);

        $demo = new NoRepositoryModel($entityManger->reveal());

    }
}
