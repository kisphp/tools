<?php

namespace tests\Model\Helper;

use Kisphp\Entity\KisphpEntityInterface;

class DemoStatusEntity implements KisphpEntityInterface
{
    /**
     * @var int
     */
    protected $status = 1;

    /**
     * @param int $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return 1;
    }
}
