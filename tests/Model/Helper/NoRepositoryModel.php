<?php

namespace tests\Model\Helper;

use Kisphp\Model\AbstractModel;

class NoRepositoryModel extends AbstractModel
{
    public function createEntity()
    {
    }
}
