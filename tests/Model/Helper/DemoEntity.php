<?php

namespace tests\Model\Helper;

use Kisphp\Entity\KisphpEntityInterface;

class DemoEntity implements KisphpEntityInterface
{
    public function getId()
    {
        return 1;
    }
}
