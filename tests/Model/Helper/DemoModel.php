<?php

namespace tests\Model\Helper;

use Kisphp\Entity\KisphpEntityInterface;
use Kisphp\Model\AbstractModel;

class DemoModel extends AbstractModel
{
    const REPOSITORY = 'Demo';

    /**
     * @return DemoEntity|KisphpEntityInterface
     */
    public function createEntity()
    {
        return new DemoEntity();
    }
}
