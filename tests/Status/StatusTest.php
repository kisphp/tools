<?php

namespace tests\Status;

use Kisphp\Exceptions\StatusCaseNotAvailable;
use Kisphp\Utils\Status;
use PHPUnit\Framework\TestCase;

class StatusTest extends TestCase
{
    public function test_status_deleted()
    {
        self::assertEquals(0, Status::DELETED);
    }

    public function test_status_inactive()
    {
        self::assertEquals(1, Status::INACTIVE);
    }

    public function test_status_active()
    {
        self::assertEquals(2, Status::ACTIVE);
    }

    public function test_get_status_deleted_html()
    {
        self::assertSame('<span class="label label-danger">Deleted</span>', Status::getStatus(0));
    }

    public function test_get_status_inactive_html()
    {
        self::assertSame('<span class="label label-warning">Inactive</span>', Status::getStatus(1));
    }

    public function test_get_status_active_html()
    {
        self::assertSame('<span class="label label-success">Active</span>', Status::getStatus(2));
    }

    public function test_status_exception()
    {
        $this->expectException(StatusCaseNotAvailable::class);
        Status::getStatus(-1);
    }
}
