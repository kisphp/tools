<?php

namespace tests\Slack\Helpers;

use Kisphp\Slack\AbstractSlackAlerter;

class SlackAlerterHelper extends AbstractSlackAlerter
{
    const SLACK_WEBHOOK_URL = 'http://www.example.com';
}
