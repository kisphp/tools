<?php

namespace tests\Slack;

use GuzzleHttp\ClientInterface;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;
use Psr\Http\Message\ResponseInterface;
use tests\Slack\Helpers\SlackAlerterHelper;
use tests\Slack\Helpers\SlackAlerterThrowsExceptionHelper;

class SlackAlerterTest extends TestCase
{
    protected function getClient()
    {
        $result = $this->createMock(ResponseInterface::class);

        $client = $this->createMock(ClientInterface::class);
        $client->method('request')->withAnyParameters()->willReturn($result);

        return $client;
    }

    public function test_slack_alerter()
    {
        $client = $this->getClient();

        $alerter = new SlackAlerterHelper($client);

        $a = $alerter->send('hello world');

        self::assertInstanceOf(ResponseInterface::class, $a);
    }

    public function test_slack_alerter_no_constant_exception()
    {
        $this->expectException(\Exception::class);

        $client = $this->getClient();

        $alerter = new SlackAlerterThrowsExceptionHelper($client);
    }
}
