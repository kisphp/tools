<?php

namespace tests\Utils\Strings;

use Kisphp\Utils\Strings;
use PHPUnit\Framework\TestCase;

class String_RankNumber_Test extends TestCase
{
    public function test_rankNumber()
    {
        self::assertSame('00001', Strings::rankNumber(1));
        self::assertSame('01101', Strings::rankNumber(1101));
    }

    public function test_string_rankNgdumber()
    {
        $this->expectException(\Exception::class);
        Strings::rankNumber('string');
    }

    public function test_empty_string_rankNumber()
    {
        $this->expectException(\Exception::class);
        Strings::rankNumber('');
    }

    public function test_negativ_rankNumber()
    {
        $this->expectException(\Exception::class);
        echo Strings::rankNumber(-20);
    }
}
