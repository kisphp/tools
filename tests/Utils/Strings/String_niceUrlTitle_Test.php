<?php

namespace tests\Utils\Strings;

use Kisphp\Utils\Strings;
use PHPUnit\Framework\TestCase;

class String_niceUrlTitle_Test extends TestCase
{
    /**
     * @dataProvider spacesStringProvider
     */
    public function test_spaces_string($teacher, $student)
    {
        self::assertSame($teacher, Strings::niceUrlTitle($student));
    }

    /**
     * @return array
     */
    public function spacesStringProvider()
    {
        return [
            ['hello-world-from-php', 'Hello World, from PHP'],
            ['hello-world-from-php', ' Hello World, from PHP '],
            ['hello-world-from-php', ' Hello World, from PHP-'],
            ['hello-world-from-php', ' Hello World/from PHP,'],
            ['hello-world-from-php', ' Hello World, from PHP#'],
        ];
    }

    public function test_non_dash_change()
    {
        self::assertSame('hello_world', Strings::niceUrlTitle('hello world', null));
    }

    public function test_null_value()
    {
        self::assertSame('', Strings::niceUrlTitle(null, null));
    }

    public function test_string_trimmed_to_null()
    {
        self::assertSame('', Strings::niceUrlTitle('(/;', null));
    }

    /**
     * @dataProvider specialCharactersProvider
     */
    public function test_special_characters_changes()
    {
        self::assertSame('', Strings::niceUrlTitle('!@#$%^&*()_+'));
        self::assertSame('', Strings::niceUrlTitle(',./;\'\\[]{}"|<>?'));
        self::assertSame('a-b', Strings::niceUrlTitle('# a b '));
    }

    /**
     * @return array
     */
    public function specialCharactersProvider()
    {
        return [
            ['', '!@#$%^&*()_+'],
            ['', ',./;\'\\[]{}"|<>?'],
            ['a-b', '# a b '],
        ];
    }
}
