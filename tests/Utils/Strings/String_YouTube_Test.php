<?php

namespace tests\Utils\Strings;

use Kisphp\Utils\Strings;
use PHPUnit\Framework\TestCase;

class String_YouTube_Test extends TestCase
{
    public function test_same_youtube_url()
    {
        $url = 'https://www.youtube.com/embed/Lp9MzpnC0FE';

        $result = Strings::generateYoutubeEmbedUrl($url);

        self::assertSame('https://www.youtube.com/embed/Lp9MzpnC0FE', $result);
    }

    public function test_generate_youtube_url()
    {
        $url = 'https://www.youtube.com/watch?v=Lp9MzpnC0FE';

        $result = Strings::generateYoutubeEmbedUrl($url);

        self::assertSame('https://www.youtube.com/embed/Lp9MzpnC0FE', $result);
    }

    public function test_wrong_youtube_url()
    {
        $url = 'https://www.youtube.com/watch?video=Lp9MzpnC0FE';

        $result = Strings::generateYoutubeEmbedUrl($url);

        self::assertSame('', $result);
    }
}
