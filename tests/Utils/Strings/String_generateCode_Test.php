<?php

namespace tests\Utils\Strings;

use Kisphp\Utils\Strings;
use PHPUnit\Framework\TestCase;

class String_generateCode_Test extends TestCase
{
    public function test_generateCode_one_character()
    {
        self::assertSame('', Strings::generateCode(1));
    }

    /**
     * @dataProvider lengthProvider
     */
    public function test_generateCode($number)
    {
        $code = Strings::generateCode($number);

        self::assertEquals($number, strlen($code));
    }

    /**
     * @return array
     */
    public function lengthProvider()
    {
        return [
            [2],
            [3],
            [4],
            [5],
            [6],
            [7],
            [8],
            [9],
            [10],
        ];
    }
}
