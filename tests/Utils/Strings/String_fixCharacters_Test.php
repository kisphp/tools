<?php

namespace tests\Utils\Strings;

use Kisphp\Utils\Strings;
use PHPUnit\Framework\TestCase;

class String_fixCharacters_Test extends TestCase
{
    /**
     * @dataProvider specialCharactersProvider
     *
     * @param string $characterToChange
     * @param string $validCharacter
     */
    public function test_fixCharacters($characterToChange, $validCharacter)
    {
        self::assertSame($validCharacter, Strings::fixCharacters($characterToChange));
    }

    /**
     * @return array
     */
    public function specialCharactersProvider()
    {
        return [
            ['Î', 'I'],
            ['Ă', 'A'],
            ['Â', 'A'],
            ['î', 'i'],
            ['â', 'a'],
            ['ă', 'a'],
            ['Ş', 'S'],
            ['ş', 's'],
            ['Ţ', 'T'],
            ['ţ', 't'],
            ['È™', 's'],
            ['È›', 't'],
            ['a€¢', '&raquo;'],
        ];
    }
}
