<?php

namespace tests\Utils;

use Kisphp\Exceptions\StatusCaseNotAvailable;
use Kisphp\Utils\Status;
use PHPUnit\Framework\TestCase;

class StatusTest extends TestCase
{
    public function test_get_status()
    {
        $this->assertStringContainsString('Deleted', Status::getStatus(0));
        $this->assertStringContainsString('Inactive', Status::getStatus(1));
        $this->assertStringContainsString('Active', Status::getStatus(2));
    }

    public function test_status_does_not_exists()
    {
        $this->expectException(StatusCaseNotAvailable::class);
        $this->assertStringContainsString('Active', Status::getStatus(20));
    }
}
