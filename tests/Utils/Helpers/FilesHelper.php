<?php

namespace tests\Utils\Helpers;

use Kisphp\Utils\Files;

class FilesHelper extends Files
{
    const MAX_FILES_IN_DIRECTORY = 2;
}
