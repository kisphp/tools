<?php

namespace tests\Utils\Helpers;

use Symfony\Component\HttpFoundation\File\UploadedFile;

class DummyUploadedFile extends UploadedFile
{
    /**
     * @return bool
     */
    public function isValid(): bool
    {
        return true;
    }
}
