<?php

namespace tests\Utils;

use Kisphp\MediaBundle\Entity\MediaFile;
use Kisphp\Utils\Files;
use Kisphp\Utils\FilesManager;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use tests\Utils\Helpers\DummyUploadedFile;
use tests\Utils\Helpers\FilesHelper;
use tests\Utils\Helpers\FileType;

class FileManagerTest extends TestCase
{
    const UPLOAD_DIRECTORY = '/tmp/uploads/';

    /**
     * @var FilesManager
     */
    protected $fm;

    protected $file;

    protected function setUp(): void
    {
        parent::setUp();

        @mkdir('/tmp/filemanager/upload', 0755, true);

        $this->fm = new FilesManager('/tmp/filemanager/app', self::UPLOAD_DIRECTORY);
    }

    public function test_file_manager()
    {
        self::assertSame('/tmp/filemanager', $this->fm->getRootDirectory());
        self::assertSame(self::UPLOAD_DIRECTORY, $this->fm->getUploadDirectory());
    }

    public function test_upload_file()
    {
        $fileTarget = '/tmp/demo.jpg';

        $img = \imagecreate(10, 10);
        \imagejpeg($img, $fileTarget);

        $uploadedFile = new DummyUploadedFile($fileTarget, 'demo.jpg', 'image/jpeg', 100, 100, true);
        $fileInterface = new FileType();

        $this->file = $this->fm->uploadFile($uploadedFile, $fileInterface);

        self::assertSame('demo.jpg', $this->file->getTitle());
    }

    public function test_remove_file()
    {
        $dirPath = self::UPLOAD_DIRECTORY . 'aaa/';
        $files = \scandir($dirPath);

        if (count($files) < 3) {
            touch($dirPath . '/demo.txt');
            $files[] = 'demo.txt';
        }

        $file = new FileType();
        $file->setDirectory(self::UPLOAD_DIRECTORY . 'aaa/');
        $file->setFilename(basename($files[2]));

        self::assertTrue($this->fm->removeFile($file));
    }

    public function test_remove_file_not_exists()
    {
        $file = new FileType();
        $file->setDirectory(self::UPLOAD_DIRECTORY . 'aaa/');
        $file->setFilename('demo.jpg');

        self::assertFalse($this->fm->removeFile($file));
    }
}
