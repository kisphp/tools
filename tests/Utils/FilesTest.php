<?php

namespace tests\Utils;

use Kisphp\Utils\Files;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use tests\Utils\Helpers\FilesHelper;

class FilesTest extends TestCase
{
    /**
     * @dataProvider incDataProvider
     */
    public function test_inc($path, $expectedPath)
    {
        self::assertSame($expectedPath, Files::inc($path));
    }

    /**
     * @return array
     */
    public function incDataProvider()
    {
        return [
            ['//alfa///beta//gama/', '/alfa/beta/gama/'],
            ['/alfa//..//beta', '/alfa/../beta'],
        ];
    }

    public function test_createTargetDirectory()
    {
        if (PHP_OS !== 'Linux' && PHP_OS !== 'Darwin') {
            self::assertEquals(1, 1);

            return;
        }

        $path = '/tmp/kisphp/';
        if (!is_dir($path)) {
            mkdir($path, 0755, true);
        }

        $filePath = FilesHelper::createTargetDirectory($path);
        self::assertSame('aaa', $filePath);
        file_put_contents($path . $filePath . '/file_1.log', 'no content needed');

        $filePath = FilesHelper::createTargetDirectory($path);
        self::assertSame('aaa', $filePath);
        file_put_contents($path . $filePath . '/file_2.log', 'no content needed');

        $filePath = FilesHelper::createTargetDirectory($path);
        self::assertSame('aab', $filePath);
        file_put_contents($path . $filePath . '/file_3.log', 'no content needed');

        $filePath = FilesHelper::createTargetDirectory($path);
        self::assertSame('aab', $filePath);
        file_put_contents($path . $filePath . '/file_4.log', 'no content needed');

        $filePath = FilesHelper::createTargetDirectory($path);
        self::assertSame('aac', $filePath);
        file_put_contents($path . $filePath . '/file_5.log', 'no content needed');

        exec('rm -rf ' . $path);
    }

    /**
     * @dataProvider fileSizeProvider
     *
     * @param string $expected
     * @param int $value
     */
    public function test_fileSize($expected, $value)
    {
        self::assertSame($expected, Files::fileSize($value));
    }

    /**
     * @return array
     */
    public function fileSizeProvider()
    {
        return [
            ['0 B', 0],
            ['20 B', 20],
            ['200 B', 200],
            ['1.95 KB', 2000],
            ['19.53 KB', 20000],
            ['195.31 KB', 200000],
            ['1.91 MB', 2000000],
            ['19.07 MB', 20000000],
            ['1.86 GB', 2000000000],
            ['186.26 GB', 200000000000],
            ['1.82 TB', 2000000000000],
            ['18.19 TB', 20000000000000],
            ['181.9 TB', 200000000000000],
            ['1.78 PB', 2000000000000000],
        ];
    }

    public function test_generateFileName_jpg()
    {
        $fileTarget = '/tmp/demo.jpg';

        $img = imagecreate(10, 10);
        imagejpeg($img, $fileTarget);

        $file = new UploadedFile($fileTarget, 'demo.jpg');

        $filename = Files::generateFilename($file);

        self::assertNotSame('demo.jpg', $filename);
        self::assertStringContainsString('.jpg', $filename);

        unlink($fileTarget);
    }

    public function test_generateFileName_png()
    {
        $fileTarget = '/tmp/demo.png';

        $img = imagecreatetruecolor(10, 10);
        imagepng($img, $fileTarget);

        $file = new UploadedFile($fileTarget, 'demo.png');

        $filename = Files::generateFilename($file);

        self::assertNotSame('demo.png', $filename);
        self::assertStringContainsString('.png', $filename);

        unlink($fileTarget);
    }

    public function test_generateFileName_gif()
    {
        $fileTarget = '/tmp/demo.gif';

        $img = imagecreate(10, 10);
        imagegif($img, $fileTarget);

        $file = new UploadedFile($fileTarget, 'demo.gif');

        $filename = Files::generateFilename($file);

        self::assertNotSame('demo.gif', $filename);
        self::assertStringContainsString('.gif', $filename);

        unlink($fileTarget);
    }

    public function test_generateFileName_pdf()
    {
        $fileTarget = __DIR__ . '/Helpers/document.pdf';

        $file = new UploadedFile($fileTarget, 'document.pdf', 'application/pdf');

        $filename = Files::generateFilename($file);

        self::assertNotSame('document.pdf', $filename);
        self::assertMatchesRegularExpression('/([a-z0-9\.]+)/', $filename);
    }

    /**
     * @dataProvider getFileTitleProvider
     *
     * @param string $validator
     * @param string $sentence
     */
    public function test_getFileTitle($validator, $sentence)
    {
        self::assertSame($validator, Files::getFileTitle($sentence));
    }

    /**
     * @return array
     */
    public function getFileTitleProvider()
    {
        return [
            ['hello-world-this-is-my-text', 'Hello, World. This is my text.'],
        ];
    }
}
