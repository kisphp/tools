<?php

namespace tests\Parser;

use Kisphp\Parser\JsonParser;
use Nette\Utils\Json;
use PHPUnit\Framework\TestCase;

class JsonParserTest extends TestCase
{
    const FILE_PATH = '/tmp/dummy.json';

    protected function setUp(): void
    {
        $fileContent = [
            [
                'id' => 1,
                'name' => 'alfa',
            ],
            [
                'id' => 2,
                'name' => 'beta',
            ],
        ];
        file_put_contents(self::FILE_PATH, json_encode($fileContent));
    }

    protected function tearDown(): void
    {
        unlink(self::FILE_PATH);
    }

    public function test_json()
    {
        $parser = new JsonParser();

        $rows = $parser->parse(self::FILE_PATH);

        self::assertArrayHasKey('id', $rows[0]);
        self::assertArrayHasKey('id', $rows[1]);
    }

    public function test_ignore_fist_row()
    {
        $this->expectException(\Exception::class);

        $parser = new JsonParser();

        $parser->ignoreFirstLine();
    }
}
