<?php

namespace tests\Parser;

use Kisphp\Parser\CsvParser;
use PHPUnit\Framework\TestCase;

class CsvParserTest extends TestCase
{
    const CSV_PATH = '/tmp/dummy.csv';

    protected function setUp(): void
    {
        $csvContent = <<<EOF
t1,t2,t3
1,2,3
4,5,6
7,8,9
EOF;
        file_put_contents(self::CSV_PATH, $csvContent);
    }

    protected function tearDown(): void
    {
        unlink(self::CSV_PATH);
    }

    public function test_csv()
    {
        $parser = new CsvParser();

        $rows = $parser->parse(self::CSV_PATH);

        self::assertEquals(3, $rows[1][2]);
    }

    public function test_csv_without_header()
    {
        $parser = new CsvParser();
        $parser->ignoreFirstLine();

        $rows = $parser->parse(self::CSV_PATH);

        self::assertEquals(3, $rows[0][2]);
    }
}
