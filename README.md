# Kisphp Tools classes

[![pipeline status](https://gitlab.com/kisphp/tools/badges/main/pipeline.svg)](https://gitlab.com/kisphp/tools/-/commits/main)
[![coverage report](https://gitlab.com/kisphp/tools/badges/main/coverage.svg)](https://gitlab.com/kisphp/tools/-/commits/main)

## Strings

```php
use Kisphp\Utils\Strings;

Strings::niceUrlTitle("Hello world (123)"); // hello-world-123

Strings::rankNumber(20); // 00020
Strings::rankNumber(20, 3); // 020

Strings::generateCode(10); // xpgUrw8ZBL (always different)
Strings::generateCode(4); // NhEB (always different)
```

## Files

```php
use Kisphp\Utils\Files;

echo Files::fileSize(12345); // 12.06 KB
echo Files::fileSize(1234567); // 1.18 MB

```
