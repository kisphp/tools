<?php

namespace Kisphp\Utils;

class Strings
{
    const PAD_LENGTH = 5;

    const YOUTUBE_EMBED_URL = 'https://www.youtube.com/embed/%s';

    /**
     * @param int $id
     *
     * @throws \Exception
     *
     * @return string
     */
    public static function rankNumber($id, $padLength = self::PAD_LENGTH)
    {
        if (is_numeric($id) === false || (int) $id < 1) {
            throw new \Exception('$id is not a positive number');
        }

        return str_pad($id, $padLength, 0, STR_PAD_LEFT);
    }

    /**
     * @param string $string
     * @param string $separator
     * @param bool $lowercase
     *
     * @return string
     */
    public static function niceUrlTitle($string, $separator = 'dash', $lowercase = true)
    {
        $characterToChange = '_';
        $replaceWith = '-';

        if (is_null($string)) {
            return '';
        }

        if ($separator !== 'dash') {
            $characterToChange = '-';
            $replaceWith = '_';
        }

        $string = self::fixCharacters($string);

        $trans = [
            '&\#\d+?;' => '',
            '&\S+?;' => '',
            '\s+' => $replaceWith,
            '[^a-z0-9\&\/\-_]' => '',
            $characterToChange . '+' => $replaceWith,
            $characterToChange . '$' => $replaceWith,
            '^' . $replaceWith => $replaceWith,
            '\.+$' => '',
            '\/' => '-',
            '&' => '-',
        ];

        $string = ($lowercase === true)
            ? strtolower(strip_tags($string))
            : strip_tags($string)
        ;

        foreach ($trans as $key => $val) {
            $string = preg_replace('#' . $key . '#i', $val, $string);
        }

        $string = preg_replace('/([\\-]{2,})/', '-', $string);

        if (strpos($string, '-') === 0) {
            $string = substr($string, 1);
        }

        if (strrpos($string, '-') === \strlen($string) - 1) {
            $string = substr($string, 0, -1);
        }

        return trim(stripslashes($string));
    }

    /**
     * @param string $text
     *
     * @return string
     */
    public static function fixCharacters($text)
    {
        $tr = [
            'Î' => 'I',
            'Ă' => 'A',
            'Â' => 'A',
            'î' => 'i',
            'â' => 'a',
            'ă' => 'a',
            'Ş' => 'S',
            'ş' => 's',
            'Ţ' => 'T',
            'ţ' => 't',
            'È™' => 's',
            'È›' => 't',
            'a€¢' => '&raquo;',
        ];

        return str_replace(array_keys($tr), $tr, $text);
    }

    /**
     * @param string $youtubeUrl
     *
     * @return string
     */
    public static function generateYoutubeEmbedUrl($youtubeUrl)
    {
        if (preg_match('/https:\/\/www.youtube.com\/embed\/([a-zA-Z0-9]+)/', $youtubeUrl)) {
            return $youtubeUrl;
        }

        $url = parse_url($youtubeUrl);
        @parse_str($url['query'], $str);

        if (!empty($str['v'])) {
            return sprintf(self::YOUTUBE_EMBED_URL, $str['v']);
        }

        return '';
    }

    /**
     * @param int $characters
     *
     * @return string
     */
    public static function generateCode($characters = 6)
    {
        $dictionary = [
            'Q', 'A', 'Z', '2', 'W', 'S', 'X', '3',
            'E', 'D', 'C', '4', 'R', 'F', 'V', '5',
            'T', 'G', 'B', '6', 'Y', 'H', 'N', '7',
            'U', 'J', 'M', '8', 'K', '9', 'L', 'P',
            'q', 'a', 'z', 'w', 's', 'x', 'e', 'd',
            'c', 'r', 'f', 'v', 't', 'g', 'b', 'y',
            'h', 'n', 'u', 'j', 'm', 'i', 'k', 'p',
        ];
        $keys = array_rand($dictionary, abs($characters));
        if (\is_int($keys) || \count($keys) < 2) {
            return '';
        }

        shuffle($dictionary);
        $generatedCode = '';
        foreach ($keys as $k) {
            $generatedCode .= $dictionary[$k];
        }

        return $generatedCode;
    }
}
