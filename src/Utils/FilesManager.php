<?php

namespace Kisphp\Utils;

use Kisphp\Entity\FileInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class FilesManager
{
    /**
     * @var string
     */
    protected $rootDirectory;

    /**
     * @var string
     */
    protected $uploadDirectory;

    /**
     * @param string $rootDirectory
     * @param string $uploadDirectory
     */
    public function __construct($rootDirectory, $uploadDirectory)
    {
        $this->rootDirectory = \dirname($rootDirectory);
        $this->uploadDirectory = $uploadDirectory;
    }

    /**
     * @return string
     */
    public function getRootDirectory()
    {
        return $this->rootDirectory;
    }

    /**
     * @return string
     */
    public function getUploadDirectory()
    {
        return $this->uploadDirectory;
    }

    /**
     * @param UploadedFile $uploadedFile
     * @param FileInterface $fileInterface
     *
     * @return FileInterface
     */
    public function uploadFile(UploadedFile $uploadedFile, FileInterface $fileInterface)
    {
        $uploadDirectory = Files::inc($this->getUploadDirectory());

        $fileInterface->setDirectory(Files::createTargetDirectory($uploadDirectory));
        $fileInterface->setFilename(Files::generateFilename($uploadedFile));
        $fileInterface->setTitle($uploadedFile->getClientOriginalName());

        $this->saveFileOnDisk($uploadedFile, $fileInterface);

        return $fileInterface;
    }

    /**
     * @param FileInterface $file
     *
     * @return bool
     */
    public function removeFile(FileInterface $file)
    {
        $fileLocation = realpath($file->getDirectory() . $file->getFilename());

        if (is_file($fileLocation)) {
            return unlink($fileLocation);
        }

        return false;
    }

    /**
     * @param FileInterface $fileInterface
     *
     * @return string
     */
    protected function getTargetDirectory(FileInterface $fileInterface)
    {
        return $fileInterface->getDirectory();
    }

    /**
     * @param UploadedFile $uploadedFile
     * @param FileInterface $fileInterface
     *
     * @return \Symfony\Component\HttpFoundation\File\File
     */
    protected function saveFileOnDisk(UploadedFile $uploadedFile, FileInterface $fileInterface)
    {
        $targetDirectory = Files::inc($this->getUploadDirectory() . '/' . $this->getTargetDirectory($fileInterface));

        return $uploadedFile->move($targetDirectory, $fileInterface->getFilename());
    }
}
