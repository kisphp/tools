<?php

namespace Kisphp\Utils;

use Symfony\Component\HttpFoundation\File\UploadedFile;

class Files
{
    const MAX_FILES_IN_DIRECTORY = 2000;

    /**
     * @param string $path
     *
     * @return string
     */
    public static function inc($path)
    {
        $path = preg_replace('/(?:[\/]{2,})/', '/', $path);

        return preg_replace('/\:\//', '://', $path);
    }

    /**
     * @param UploadedFile $file
     *
     * @return string
     */
    public static function generateFilename(UploadedFile $file)
    {
        $extension = $file->getClientOriginalExtension();

        if ($file->getMimeType() === 'image/jpeg') {
            $extension = 'jpg';
        }
        if ($file->getMimeType() === 'image/png') {
            $extension = 'png';
        }
        if ($file->getMimeType() === 'image/gif') {
            $extension = 'gif';
        }
        if ($file->getMimeType() === 'image/webp') {
            $extension = 'webp';
        }

        return uniqid() . '.' . $extension;
    }

    /**
     * @param string $dirname
     *
     * @return int
     */
    public static function countFiles($dirname)
    {
        $dh = @opendir($dirname);
        $filesInDir = 0;
        while (($file = @readdir($dh)) !== false) {
            if ($file !== '.' && $file !== '..') {
                ++$filesInDir;
            }
        }
        closedir($dh);

        return $filesInDir;
    }

    /**
     * @param int $size
     *
     * @return string
     */
    public static function fileSize($size)
    {
        $unit = ['B', 'KB', 'MB', 'GB', 'TB', 'PB'];
        if ($size < 1) {
            return '0 ' . $unit[0];
        }

        $logValue = log($size, 1024);
        $floorLog = floor($logValue);
        $val = $size / pow(1024, $floorLog);

        return round($val, 2) . ' ' . $unit[(int) $floorLog];
    }

    /**
     * @param string $title
     *
     * @return string
     */
    public static function getFileTitle($title)
    {
        $title = strip_tags($title);
        $title = trim($title);
        $title = strtolower($title);
        $title = str_replace(' ', '-', $title);

        return preg_replace('/([^a-zA-Z0-9-_+]+)/', '', $title);
    }

    /**
     * @param string $parentDirectory
     *
     * @return string
     */
//    public static function createTargetDirectory($parentDirectory)
//    {
//        $characters = 'a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,0,1,2,3,4,5,6,7,8,9';
//        $charactersArray = explode(',', $characters);
//
//        $directoryName = '';
//        foreach ($charactersArray as $a) {
//            foreach ($charactersArray as $b) {
//                foreach ($charactersArray as $c) {
//                    $dirname = static::inc($parentDirectory . '/' . $a . $b . $c);
//                    if (is_dir($dirname)) {
//                        if (static::countFiles($dirname) < static::MAX_FILES_IN_DIRECTORY) {
//                            $directoryName = $a . $b . $c;
//
//                            break 3;
//                        }
//                    } else {
//                        mkdir($dirname, 0755, true);
//
//                        $directoryName = $a . $b . $c;
//
//                        break 3;
//                    }
//                }
//            }
//        }
//
//        return $directoryName;
//    }

    /**
     * @param string $parentDirectory
     *
     * @return string|null
     */
    public static function createTargetDirectory(string $parentDirectory)
    {
        $characters = 'abcdefghijklmnopqrstuvwxyz0123456789'; // String is more efficient than array for this purpose
        $charLen = strlen($characters);
        $maxCombinations = $charLen ** 3; // Calculate the maximum number of directory names

        for ($i = 0; $i < $maxCombinations; $i++) {
            $dirName = '';
            $temp = $i;

            // Efficiently generate the 3-character directory name
            for ($j = 0; $j < 3; $j++) {
                $dirName = $characters[$temp % $charLen] . $dirName;
                $temp = (int)($temp / $charLen); // Integer division
            }

            $fullPath = $parentDirectory . DIRECTORY_SEPARATOR . $dirName;

            if (!is_dir($fullPath)) {
                if (mkdir($fullPath, 0755, true)) { // Check if mkdir was successful
                    return $dirName;
                } else {
                    // Handle mkdir failure (log, throw exception, etc.)
                    error_log("Failed to create directory: " . $fullPath);
                    return null; // Or throw an exception
                }
            } else {
                if (static::countFiles($fullPath) < static::MAX_FILES_IN_DIRECTORY) {
                    return $dirName;
                }
            }
        }

        // Handle the case where no suitable directory could be found
        error_log("No suitable directory could be found in " . $parentDirectory);
        return null; // Or throw an exception
    }

    /**
     * Generates a sequence of directories in the format "aaa", "aab", "aac", etc.,
     * ensuring a maximum of 1000 files (represented by placeholder files) per directory.
     *
     * @param string $baseDir The root directory where the sequence will be created.
     * @param int $maxFilesPerDir The maximum number of files allowed per directory.
     * @param int $numDirsToGenerate The number of directories to generate. Defaults to 1.
     * @return array An array containing the generated directory paths.  Returns an empty
     *               array if there are issues creating the directories.
     * @throws Exception If the base directory is not writable.
     */
//    public static function generateDirectorySequence(string $baseDir, int $maxFilesPerDir = 10, int $numDirsToGenerate = 1): array
//    {
//        if (!is_writable($baseDir)) {
//            throw new \Exception("Base directory '{$baseDir}' is not writable.");
//        }
//
//        $directories = [];
//        $counter = 0;
//
//        for ($i = 0; $i < $numDirsToGenerate; $i++) {
//            $dirName = '';
//            $tempCounter = $counter;
//
//            // Generate the 3-character directory name (e.g., "aaa", "aab", "aac")
//            for ($j = 0; $j < 3; $j++) {
//                $charIndex = $tempCounter % 26;
//                $dirName = chr(97 + $charIndex) . $dirName; // 97 is ASCII for 'a'
//                $tempCounter = (int)($tempCounter / 26);  // Integer division for correct character progression
//            }
//
//
//            $fullPath = $baseDir . DIRECTORY_SEPARATOR . $dirName;
//
//            if (!is_dir($fullPath)) { // Check if the directory exists before attempting to create
//                if (!mkdir($fullPath, 0777, true)) { // Create the directory recursively
//                    error_log("Failed to create directory: " . $fullPath); // Log the error
//                    return []; // Or handle the error as needed
//                }
//            }
//
//            $directories[] = $fullPath;
//
//            // Create placeholder files within the directory (if it doesn't have enough)
//            $existingFileCount = count(glob($fullPath . DIRECTORY_SEPARATOR . '*')); // Efficient way to count files
//
//            for ($fileNum = $existingFileCount; $fileNum < $maxFilesPerDir; $fileNum++) {
//                touch($fullPath . DIRECTORY_SEPARATOR . "file_{$fileNum}.txt"); // Create placeholder files
//            }
//
//            $counter++; // Increment for the next directory name
//        }
//
//        return $directories;
//    }

}
