<?php

namespace Kisphp\Entity;

interface ToggleableInterface
{
    /**
     * @param int $status
     *
     * @return $this
     */
    public function setStatus($status);

    /**
     * @return int
     */
    public function getStatus();
}
