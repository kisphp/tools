<?php

namespace Kisphp\Entity;

interface KisphpEntityInterface
{
    /**
     * @return int
     */
    public function getId();
}
