<?php

namespace Kisphp\Entity;

interface AttachedInterface
{
    /**
     * @return string
     */
    public function getDirectory();

    /**
     * @return string
     */
    public function getFilename();
}
