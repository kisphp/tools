<?php

namespace Kisphp\Model;

use Doctrine\ORM\EntityManagerInterface;
use Kisphp\Entity\KisphpEntityInterface;
use Kisphp\Entity\ToggleableInterface;
use Kisphp\Exceptions\ConstantNotFound;
use Kisphp\Utils\Status;

abstract class AbstractModel
{
    /** Rewrite this constant in your model */
    const REPOSITORY = '';

    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @param EntityManagerInterface $em
     *
     * @throws ConstantNotFound
     */
    public function __construct(EntityManagerInterface $em)
    {
        if (empty(static::REPOSITORY)) {
            $message = sprintf('You must declare REPOSITORY constant in %s', static::class);

            throw new ConstantNotFound($message);
        }

        $this->em = $em;
    }

    /**
     * @return KisphpEntityInterface
     */
    abstract public function createEntity();

    /**
     * @param KisphpEntityInterface $entity
     *
     * @return KisphpEntityInterface
     */
    public function save(KisphpEntityInterface $entity)
    {
        $this->persist($entity);
        $this->flush();

        return $entity;
    }

    /**
     * @param KisphpEntityInterface $entity
     */
    public function persist(KisphpEntityInterface $entity)
    {
        $this->em->persist($entity);
    }

    public function flush()
    {
        $this->em->flush();
    }

    /**
     * @param int $id
     *
     * @return object|null
     */
    public function find($id)
    {
        return $this
            ->getRepository()
            ->find($id)
        ;
    }

    /**
     * @param int $id
     *
     * @return KisphpEntityInterface
     */
    public function findOneOrCreateById($id)
    {
        $entity = $this->find($id);

        if (empty($entity)) {
            $entity = $this->createEntity();
        }

        return $entity;
    }

    /**
     * @param array $criteria
     *
     * @return object|null
     */
    public function findOneBy(array $criteria)
    {
        return $this->getRepository()
            ->findOneBy($criteria)
        ;
    }

    /**
     * @param array $criteria
     * @param array|null $orderBy
     * @param null $limit
     * @param null $offset
     *
     * @throws \UnexpectedValueException
     *
     * @return array
     */
    public function findBy(array $criteria, array $orderBy = [], $limit = null, $offset = null)
    {
        return $this->getRepository()
            ->findBy($criteria, $orderBy, $limit, $offset)
        ;
    }

    /**
     * @return array
     */
    public function findAll()
    {
        return $this
            ->getRepository()
            ->findAll()
        ;
    }

    /**
     * @param KisphpEntityInterface|ToggleableInterface $entity
     *
     * @return KisphpEntityInterface
     */
    public function remove(KisphpEntityInterface $entity)
    {
        if (method_exists($entity, 'setStatus')) {
            $entity->setStatus(Status::DELETED);

            return $this->save($entity);
        }

        $this->em->remove($entity);
        $this->em->flush();

        return $entity;
    }

    /**
     * @return \Doctrine\Common\Persistence\ObjectRepository
     */
    protected function getRepository()
    {
        return $this->em
            ->getRepository(static::REPOSITORY)
        ;
    }
}
