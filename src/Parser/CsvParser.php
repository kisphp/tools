<?php

namespace Kisphp\Parser;

class CsvParser implements ParserInterface
{
    /**
     * @var bool
     */
    protected $ignoreFirstLine = false;

    /**
     * @return CsvParser
     */
    public function ignoreFirstLine()
    {
        $this->ignoreFirstLine = true;

        return $this;
    }

    /**
     * @param string $fileTarget
     *
     * @return array
     */
    public function parse($fileTarget)
    {
        $rows = [];
        $handle = fopen($fileTarget, 'r');
        if ($handle) {
            $i = 0;
            while (($data = fgets($handle)) !== false) {
                ++$i;
                if ($this->ignoreFirstLine && $i === 1) {
                    continue;
                }
                $rows[] = str_getcsv($data, ',', '"', '\\');
            }
            fclose($handle);
        }

        return $rows;
    }
}
