<?php

namespace Kisphp\Parser;

class JsonParser implements ParserInterface
{
    /**
     * @throws \LogicException
     */
    public function ignoreFirstLine()
    {
        throw new \LogicException('Ignore First Line not aplyable for JSON files');
    }

    /**
     * @param string $fileTarget
     *
     * @return array
     */
    public function parse($fileTarget)
    {
        $content = file_get_contents($fileTarget);

        return json_decode($content, true);
    }
}
