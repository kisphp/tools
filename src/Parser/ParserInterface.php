<?php

namespace Kisphp\Parser;

interface ParserInterface
{
    /**
     * @return ParserInterface
     */
    public function ignoreFirstLine();

    /**
     * @param string $fileTarget
     *
     * @return array
     */
    public function parse($fileTarget);
}
