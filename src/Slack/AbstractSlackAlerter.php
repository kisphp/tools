<?php

namespace Kisphp\Slack;

use GuzzleHttp\ClientInterface;

abstract class AbstractSlackAlerter
{
    const SLACK_WEBHOOK_URL = null;
    const SLACK_BOT_NAME = 'Slack';

    /**
     * @var ClientInterface
     */
    protected $client;

    /**
     * @param ClientInterface $client
     *
     * @throws \Exception
     */
    public function __construct(ClientInterface $client)
    {
        $this->client = $client;

        if (static::SLACK_WEBHOOK_URL === null) {
            throw new \Exception('You must define constant SLACK_WEBHOOK_URL in your concrete class from the project');
        }
    }

    /**
     * @param string $message
     * @param string $channelOrUser
     * @param mixed $icon
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function send($message, $channelOrUser = '#general', $icon = ':mailbox:')
    {
        $response = $this->client->request('POST', $this->getSlackWebHookUrl(), [
            'json' => [
                'channel' => $channelOrUser,
                'username' => $this->getSlackBotName(),
                'text' => $message,
                'icon_emoji' => $icon,
            ],
        ]);

        return $response;
    }

    /**
     * @return null
     */
    protected function getSlackWebHookUrl()
    {
        return static::SLACK_WEBHOOK_URL;
    }

    /**
     * @return string
     */
    protected function getSlackBotName()
    {
        return static::SLACK_BOT_NAME;
    }
}
